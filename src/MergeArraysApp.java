import java.util.Arrays;
import java.util.stream.Stream;

public class MergeArraysApp {
    public static void main(String[] args) {
        int[] merged = merge(
                new int[]{4, 103, 104},
                new int[]{1, 2},
                new int[]{3, 99, 104},
                new int[]{17, 18, 19, 20, 21, 22, 23, 24},
                new int[]{5, 12, 14, 23},
                new int[]{1, 3, 29, 97});

        System.out.println("merged = " + Arrays.toString(merged));
    }

    private static int[] merge(int[]... arrays) {
        int length = Stream.of(arrays).mapToInt(a -> a.length).sum();

        int[] result = new int[length];
        int[] position = new int[arrays.length];
        Arrays.fill(position, 0);

        for (int i = 0; i < length; i++) {
            int[] elements = currentElements(arrays, position);
            Element minElement = findMin(elements);
            result[i] = minElement.minValue;
            position[minElement.arrayNumber]++;
        }
        return result;
    }

    private static int[] currentElements(int[][] arrays, int[] position) {
        int[] result = new int[arrays.length];
        for (int i = 0; i < arrays.length; i++) {
            result[i] = arrays[i].length > position[i] ? arrays[i][position[i]] : Integer.MAX_VALUE;
        }
        return result;
    }

    private static Element findMin(int[] array) {
        int p = 0;
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < array.length; i++) {
            min = Math.min(min, array[i]);
            p = (array[i] == min) ? i : p;
        }
        return new Element(p, min);
    }

    static class Element {
        int arrayNumber;
        int minValue;

        Element(int arrayNumber, int minValue) {
            this.arrayNumber = arrayNumber;
            this.minValue = minValue;
        }
    }
}
